##### ftp-music-sync
A small Python client to sync music over an FTP server.

Will likely support many configurations and possibly dynamic DNS in the future.

Requires numpy

# **NOTE:**
This script needs a `config.json` file in the working directory to function properly.
It will be generated on start if it does not exist. You will need to edit it to configure the program.