import ftplib
import json
import math
import os
import platform
import re
import sys
import time
try:
    from tkinter import Tk
    from tkinter import messagebox as alert
except Exception:
    pass

import numpy as np

config_base = {"ftp_host": "", "ftp_port": 21, "ftp_user": "", "ftp_pass": "", "ftp_use_tls": False,
               "ftp_is_pasv": True, "ftp_target_working_dir": "", "timed_sync_frequency": 3600,
               "sync_on_local_change": True, "local_music_dirs": [], "allowed_extensions": [], "ask_filename_fix": True,
               "ask_dupe_fix": True}

toast_enabled = False
notifier = None

ftp = None

print('Starting Python Music Sync service.')

if os.path.isfile('config.json'):
    config = json.load(open('config.json', 'r'))
else:
    config = config_base
    jsonfile = open('config.json', 'w+')
    json.dump(config_base, jsonfile)
    jsonfile.close()


class ProgressTracker():
    def __init__(self, total, start=0):
        self.total = total
        self.start = start
        self.current = start

    def changeprogress(self, mod):
        self.current += mod

    def setprogress(self, total):
        self.current = total

    def getprogresspercent(self):
        return float(self.current) / float(self.total)

    def printprogressbar(self):
        percent = ("{0:." + str(1) + "f}").format(100 * self.getprogresspercent())
        filled_length = int(20 * self.getprogresspercent())
        bar = '█' * filled_length + '-' * (20 - filled_length)
        print('\r%s |%s| %s%% %s' % ('<', bar, percent, '>'), end='')


def save_config():
    json.dump(config, open('config.json', 'w+'))


def toast(title, message):
    if toast_enabled:
        notifier.show_toast(title=title, msg=message, duration=5, threaded=True)


def check_config():
    if type(config['ftp_host']) is not str or config['ftp_host'] == "":
        passed = False
        toast("Python Music Sync",
              "A property in the config is improperly set. Please check the console to reconfigure.")
        while not passed:
            print('Invalid FTP host in config.')
            host = input("Input new FTP host: ")
            if re.compile(
                    r'(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])').search(
                host):
                passed = True
                config['ftp_host'] = host
                save_config()
    if type(config['ftp_target_working_dir']) is not str or config['ftp_target_working_dir'] == "":
        print('WARNING: FTP working dir is invalid. Files will be synced in FTP root.')
        toast("Python Music Sync",
              "WARNING: FTP working dir is invalid. Files will be synced in FTP root.")
    if config['local_music_dirs'] == [] or type(config['local_music_dirs']) is not list:
        print('WARNING: Local music dirs are invalid. Files will be synced in script working directory.')
        toast("Python Music Sync",
              "WARNING: Local music dirs are invalid. Files will be synced in script working directory.")
    elif len(config['local_music_dirs']) > 1:
        print('INFO: Multiple local dirs specified. Remote files will be copied into the first dir listed.')
    if config['allowed_extensions'] == [] or type(config['allowed_extensions']) is not list:
        print('WARNING: Allowed extensions property is invalid. All files in working dir will be synced.')
        toast("Python Music Sync",
              "WARNING: Allowed extensions property is invalid. All files in working dir will be synced.")
    if type(config['ftp_port']) is not int or config['ftp_port'] < 1:
        passed = False
        toast("Python Music Sync",
              "A property in the config is improperly set. Please check the console to reconfigure.")
        while not passed:
            print('Invalid FTP port in config.')
            port = input("Input new FTP port: ")
            if port.isnumeric() and '.' not in port:
                passed = True
                config['ftp_port'] = port
                save_config()


def check_extension(path):
    if path == 'config.json' or path == os.path.basename(sys.argv[0]):
        return False
    if config['allowed_extensions'] == [] or type(config['allowed_extensions']) is not list:
        return True
    else:
        for ext in config['allowed_extensions']:
            if path.endswith('.' + str(ext)):
                return True
    return False


def ftp_connect():
    global ftp
    try:
        print('Connecting to ftp://' + config['ftp_host'])
        ftp = ftplib.FTP_TLS(config['ftp_host'])
        # ftp.set_debuglevel(2)
        ftp.port = config['ftp_port']
        ftp.login(user=config['ftp_user'], passwd=config['ftp_pass'])
        if config['ftp_is_pasv']:
            ftp.set_pasv(True)
        if config['ftp_target_working_dir']:
            ftp.cwd(config['ftp_target_working_dir'])
        if config['ftp_use_tls']:
            ftp.prot_p()
        ftp.voidcmd('noop')
        print('Connected to ftp://' + config['ftp_host'])
    except Exception as e:
        toast("Python Music Sync", "FTP connection failed. Check your configuration.")
        print(e)
        print("Connection failed. Check your configuration.")


file_to_write = None
block_size = 8192


def retr_bin_file(buffer):
    global file_to_write
    if not hasattr(file_to_write, 'write') or file_to_write is None:
        raise IOError('Target output file is not defined!')
    else:
        file_to_write.write(buffer)
        if len(buffer) < block_size:
            file_to_write.close()
            file_to_write = None
    ftp_check_and_revive()


def first_local_dir():
    if len(config['local_music_dirs']) > 0:
        return config['local_music_dirs'][0]
    else:
        return ''


last_poll_files = None
local_mod_files = []
deleted_files = []


def make_transmitable_str(string):
    # print('original: ' + string)
    # print('new: ' + str(''.join([c if re.search(r'([ -~])+', c) else '_' for c in string])))
    return str(''.join([c if re.search(r'([ -~])+', c) else '_' for c in string]))


def ftp_check_and_revive():
    try:
        ftp.voidcmd('noop')
    except Exception as e:
        print(e)
        print('Server closed connection. Resetting FTP connection.')
        ftp.close()
        ftp_connect()


def poll_files():
    global last_poll_files
    current_poll_files = []
    if first_local_dir() == '':
        for local_file in [f for f in os.listdir() if os.isfile(f) and check_extension(f)]:
            current_poll_files.append(local_file)
    else:
        for music_dir in config['local_music_dirs']:
            for local_file in [f for f in os.listdir(music_dir) if
                               os.path.isfile(os.path.join(music_dir, f)) and check_extension(f)]:
                current_poll_files.append(os.path.join(music_dir, local_file))
    do_sync = False
    for cpf in current_poll_files:
        if last_poll_files is None:
            break
        last_fnames = [lastfile[0] for lastfile in last_poll_files]
        if cpf not in last_fnames:
            print('new file: ' + cpf)
            do_sync = True
        else:
            lastfile_idx = last_fnames.index(cpf)
            if os.stat(cpf).st_mtime != last_poll_files[lastfile_idx][1]:
                print('modified: ' + cpf)
                local_mod_files.append(cpf)
                do_sync = True
    if last_poll_files is not None:
        for lpf in [lastfile[0] for lastfile in last_poll_files]:
            if lpf not in current_poll_files:
                deleted_files.append(lpf)
                print('deleted file: ' + lpf)
                do_sync = True
    trimmed_transmitable_filenames = []
    trimmed_files = [os.path.basename(lf) for lf in current_poll_files]
    for tf in trimmed_files:
        trimmed_transmitable_filenames.append(make_transmitable_str(tf))
    ftp_check_and_revive()
    remote_files = ftp.nlst()
    for remote_f in remote_files:
        if remote_f not in trimmed_transmitable_filenames:
            print('remote new file: ' + remote_f)
            do_sync = True
    for local_file in current_poll_files:
        if os.path.basename(local_file) not in remote_files:
            print('local new file: ' + local_file)
            do_sync = True
    last_poll_files = []
    for cpf in current_poll_files:
        last_poll_files.append((cpf, os.stat(cpf).st_mtime))
    if do_sync and current_poll_files != []:
        sync_once()


def scan_and_fix_filenames():
    local_files = []
    if config['local_music_dirs'] == [] or type(config['local_music_dirs']) is not list:
        for file in os.listdir():
            if os.path.isfile(file):
                if check_extension(file) and file != 'config.json' and file != os.path.basename(sys.argv[0]):
                    local_files.append(file)
    else:
        for music_dir in config['local_music_dirs']:
            for file in os.listdir(music_dir):
                file_abs_path = os.path.join(music_dir, file)
                if os.path.isfile(file_abs_path):
                    if check_extension(file_abs_path) and file != 'config.json' and file != os.path.basename(
                            sys.argv[0]):
                        local_files.append(file_abs_path)
    trimmed_files = []
    if config['local_music_dirs'] == [] or type(config['local_music_dirs']) is not list:
        trimmed_files = np.vstack((local_files, local_files))
    else:
        for local_file in local_files:
            trimmed_file = os.path.basename(local_file)
            trimmed_files.append(trimmed_file)
        trimmed_files = np.vstack((trimmed_files, local_files))
    tracker = ProgressTracker(len(trimmed_files[0]))
    print('Checking Filenames:')
    for tf in trimmed_files[0]:
        tracker.changeprogress(1)
        tracker.printprogressbar()
        for fname_char in tf:
            if not re.search(r'([ -~])+', fname_char) and fname_char not in '/\\?%*:|"<>':
                try:
                    reply_bool = alert.askyesno("Confirmation needed",
                                                "One or more of the files in your library contains unsupported "
                                                "characters in their names. In order to ensure the same files are "
                                                "not transferred multiple times, these files should have the "
                                                "special characters removed from their names. Would you like "
                                                "to do this automatically?")
                except Exception:
                    print("One or more of the files in your library contains unsupported "
                          "characters in their names. In order to ensure the same files are "
                          "not transferred multiple times, these files should have the "
                          "special characters removed from their names.")
                    reply = input("Would you like to do this automatically? (y/n) ").lower()
                    reply_bool = (reply == 'yes' or reply == 'y')
                if reply_bool:
                    for local_file in local_files:
                        parent_dir = os.path.dirname(local_file)
                        basename = os.path.basename(local_file)
                        basename = make_transmitable_str(basename)
                        os.rename(local_file, os.path.join(parent_dir, basename))
                else:
                    return
    print('')


def remove_duplicates():
    from difflib import SequenceMatcher
    import itertools
    local_files = []
    if config['local_music_dirs'] == [] or type(config['local_music_dirs']) is not list:
        for file in os.listdir():
            if os.path.isfile(file):
                if check_extension(file) and file != 'config.json' and file != os.path.basename(sys.argv[0]):
                    local_files.append(file)
    else:
        for music_dir in config['local_music_dirs']:
            for file in os.listdir(music_dir):
                file_abs_path = os.path.join(music_dir, file)
                if os.path.isfile(file_abs_path):
                    if check_extension(file_abs_path) and file != 'config.json' and file != os.path.basename(
                            sys.argv[0]):
                        local_files.append(file_abs_path)
    dupe_files = []
    tracker = ProgressTracker(
        int(math.factorial(len(local_files))) // int(2) // int(math.factorial(len(local_files) - 2)))
    print('Scanning for duplicates:')
    for a, b in itertools.combinations(local_files, 2):
        tracker.changeprogress(1)
        tracker.printprogressbar()
        if SequenceMatcher(None, a, b).real_quick_ratio() > 0.7:
            dupe_files.append((a, b))
    if len(dupe_files) > 0:
        message_build = "One or more files was detected to have duplicate names:\r\n"
        for dupe_file in dupe_files:
            message_build += "  " + str(dupe_file[0]) + " --- " + str(dupe_file[1]) + "\r\n"
        message_build += "Would you like to remove the duplicate files?"
        try:
            reply_bool = alert.askyesno("Confirmation needed", message_build)
        except Exception:
            print(message_build, end=' ')
            reply = input("(y/n) ").lower()
            reply_bool = (reply == 'yes' or reply == 'y')
        if reply_bool:
            for dupe_file in dupe_files:
                os.remove(dupe_file[1])
        else:
            return
    print('')


def sync_once():
    global file_to_write, local_mod_files, deleted_files
    toast("Python Music Sync", "Syncing local music...")
    print("Syncing local music...")
    local_files = []
    if config['local_music_dirs'] == [] or type(config['local_music_dirs']) is not list:
        for file in os.listdir():
            if os.path.isfile(file):
                if check_extension(file) and file != 'config.json' and file != os.path.basename(sys.argv[0]):
                    local_files.append(file)
    else:
        for music_dir in config['local_music_dirs']:
            for file in os.listdir(music_dir):
                file_abs_path = os.path.join(music_dir, file)
                if os.path.isfile(file_abs_path):
                    if check_extension(file_abs_path) and file != 'config.json' and file != os.path.basename(
                            sys.argv[0]):
                        local_files.append(file_abs_path)
    ftp_connect()
    print(ftp.getwelcome())
    print('FTP Working dir: ' + str(ftp.pwd()))
    if config['ftp_use_tls']:
        ftp.prot_p()
    for deleted_file in deleted_files:
        ftp.delete(make_transmitable_str(os.path.basename(deleted_file)))
    remote_files = ftp.nlst()
    deleted_files = []
    delta_files = []
    local_indexes = []
    trimmed_transmitable_filenames = []
    trimmed_files = [os.path.basename(lf) for lf in local_files]
    for tf in trimmed_files:
        trimmed_transmitable_filenames.append(make_transmitable_str(tf))
    for local_f in trimmed_files:
        localidx = trimmed_files.index(local_f)
        local_indexes.append(localidx)
        if make_transmitable_str(local_f) not in remote_files:
            delta_files.append((local_f, 'local', localidx))
    for remote_f in remote_files:
        if os.path.basename(remote_f) not in trimmed_transmitable_filenames:
            delta_files.append((remote_f, 'remote'))
    for local_mod_file in local_mod_files:
        localidx = trimmed_files.index(os.path.basename(local_mod_file))
        delta_files.append((os.path.basename(local_mod_file), 'local', localidx))
    local_mod_files = []
    print('Files to sync: ' + str(delta_files) + ' (' + str(len(delta_files)) + ')')
    for delta_file in delta_files:
        if delta_file[1] == 'local':
            try:
                ftp_check_and_revive()
                ftp.prot_p()
                ftp.storbinary('STOR ' + make_transmitable_str(delta_file[0]),
                               open(local_files[delta_file[2]], 'rb'), blocksize=block_size,
                               callback=ftp_check_and_revive())
            except Exception as e:
                print(e)
                print('Failed to upload file ' + make_transmitable_str(delta_file[0]))
                toast("Python Music Sync", 'Failed to upload file ' + delta_file[0])
        elif delta_file[1] == 'remote':
            try:
                file_to_write = open(os.path.join(first_local_dir(), delta_file[0]), 'wb+')
                ftp_check_and_revive()
                ftp.retrbinary('RETR ' + str(delta_file[0]), retr_bin_file, blocksize=block_size)
            except Exception as e:
                print(e)
                print('Failed to transfer file ' + delta_file[0] + '. ' + str(e))
    ftp.quit()


if __name__ == '__main__':
    if 'Windows' in platform.system() and '10' in platform.release():
        toast_enabled = True
        import win10toast

        notifier = win10toast.ToastNotifier()
        notifier.show_toast("Python Music Sync", "Sync service starting.", threaded=True)
    else:
        print('Not Windows 10, toast notifications will not be enabled.')

    try:
        mb_host_window = Tk()
        mb_host_window.withdraw()
    except Exception:
        print('Could not create a root window, must be command line only.')

    check_config()
    if config['ask_dupe_fix']:
        remove_duplicates()
    if config['ask_filename_fix']:
        scan_and_fix_filenames()
    sync_once()

    realtime_poll_interval = 1000

    while 1:
        if config['sync_on_local_change']:
            time.sleep(realtime_poll_interval / 1000)
        else:
            time.sleep(config['timed_sync_frequency'])
        poll_files()
